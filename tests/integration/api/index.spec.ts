import request from 'supertest'
import {app} from '../../../src/app'
import * as fs from "fs";
import FormData from "form-data";
import {TEST_PATH} from "../../../src/configs";

const testDataPath = TEST_PATH + '/data/MOCK_DATA_10.csv';

describe("API Integration Tests", () => {
    describe("POST /api/upload", () => {

        it("#Test authorization expect 401", async () => {
            const response = await request(app).post("/api/upload")
            expect(response.statusCode).toEqual(401)
            expect(response.body).toHaveProperty("message")
        })

        it('#Test authorization expect 400', async () => {
            const response = await request(app).post("/api/upload")
                .set({'Authorization': 'Bearer 123'});
            expect(response.statusCode).toEqual(400)
            expect(response.body).toHaveProperty("message")
            expect(response.body.message).toEqual("No file uploaded")
        })

        it('#Test upload file expect 201', async () => {
            const formData = new FormData();
            formData.append('file', fs.createReadStream(testDataPath));
            const response = await request(app).post("/api/upload")
                .set({'Authorization': 'Bearer 123'})
                .field('email', 'ronny@fretelweb.com')
                .attach('file', testDataPath);
            expect(response.statusCode).toEqual(201)
            expect(response.body).toHaveProperty('data.token')
            expect(response.body.data.token).not.toEqual('')

            // Wait for background mailing process
            const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
            await sleep(10000);

        }, 20000);

    })
})

import {getMockReq, getMockRes} from "@jest-mock/express";
import {authenticate} from "../../../../src/app/middlewares";

describe("Authentication Middleware", () => {

    describe("Authenticate register partner", () => {
        beforeEach(() => mockClear())

        const {res, next, mockClear} = getMockRes()

        it("#Request has Authorization token", () => {
            const req = getMockReq({headers: {authorization: "Bearer 123456"}});
            authenticate(req, res, next);
            expect(res.status).toHaveBeenCalledTimes(0);
            expect(next).toHaveBeenCalled();
        })

        it("#Request has no Authorization token", () => {
            const req = getMockReq();
            authenticate(req, res, next);
            expect(res.status).toHaveBeenCalledWith(401);
            expect(next).not.toHaveBeenCalled();
        })

    });

});

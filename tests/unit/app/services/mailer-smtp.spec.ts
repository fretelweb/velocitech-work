import * as fs from "fs";
import {TEST_PATH} from "../../../../src/configs";
import {MailerSmtp} from "../../../../src/app/services";

const testDataPath = TEST_PATH + '/data/MOCK_DATA_1000.csv'

describe('MailerSmtp', () => {

    it('#Test MailerSMTP delivery with Attachment', async () => {
        const mailer = new MailerSmtp();
        const response = await mailer.sendMail(
            'ronny@fretelweb.com',
            'Test MailerSMTP with Attachment',
            'This is a test email from jest',
            fs.createReadStream(testDataPath)
        )
        expect(response).toBeTruthy();
        expect(response.response).toEqual('250 Requested mail action okay, completed');
    }, 10000);
});
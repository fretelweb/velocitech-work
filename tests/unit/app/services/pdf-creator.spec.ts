import * as fs from "fs";
import {Certificate, Roles} from "../../../../src/domain/models";
import {pdfCreator} from "../../../../src/app/services";
import {UPLOAD_PATH} from "../../../../src/configs";

describe('UNIT TEST PDF CREATOR', () => {
    it('#Test createPDF expect pdf created', async () => {
        const data: Certificate = {
            Email: 'test@gmail.com',
            Username: 'test',
            Password: 'test',
            FirstName: 'Dorolisa',
            LastName: 'Giacomazzi',
            Role: Roles.CYCLIST,
            Age: 18,
            LicenseRegistrationDate: new Date(),
            LicenseExpirationDate: new Date(),
            LicenseCode: 'test-1234567890-1234234-234-234'
        }

        pdfCreator(data);
        const pdfPath = `${UPLOAD_PATH}/pdf/${data.LicenseCode}.pdf`;
        const exist = fs.existsSync(pdfPath);
        expect(exist).toEqual(true);
        fs.unlinkSync(pdfPath);

    })
})
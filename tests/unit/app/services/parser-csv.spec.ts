import {TEST_PATH} from "../../../../src/configs";
import {parserCsv} from "../../../../src/app/services";

const mockData10 = TEST_PATH + '/data/MOCK_DATA_10.csv';
const mockData1000 = TEST_PATH + '/data/MOCK_DATA_1000.csv';


describe("UNIT TEST PARSER CSV", () => {
    it("#Test parseCSV expect 1000", async () => {
        const results = await parserCsv(mockData1000);
        expect(results.length).toEqual(1000);
    })
    it("#Test parseCSV expect 10", async () => {
        const results = await parserCsv(mockData10);
        expect(results.length).toEqual(10);
    })
})
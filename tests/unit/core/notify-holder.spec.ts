import {Mailer} from "../../../src/domain/interfaces";
import {Certificate, Roles} from "../../../src/domain/models";
import {notifyHolder} from "../../../src/core";
import {pdfCreator} from "../../../src/app/services";
import {UPLOAD_PATH} from "../../../src/configs";
import * as fs from "fs";

describe('NotifyPartner', () => {

    it('#Notify Certificate Holder ', async () => {
        const mailer: Mailer | any = {
            sendMail: jest.fn()
        };
        const data: Certificate = {
            Age: 30,
            Email: "ronny@fretelweb.com",
            FirstName: "Ronny",
            LastName: "Fretel",
            LicenseCode: "test-76432-234-234-234",
            LicenseExpirationDate: new Date(),
            LicenseRegistrationDate: new Date(),
            Password: "sad34534",
            Role: Roles.CYCLIST,
            Username: "RFretel"
        }
        const pdfPath = pdfCreator(data);
        const pdfAttach = fs.createReadStream(pdfPath);
        await notifyHolder(data, pdfAttach, mailer);
        
        expect(mailer.sendMail).toHaveBeenCalled();
        expect(mailer.sendMail.mock.calls[0][0]).toEqual('ronny@fretelweb.com');
        expect(mailer.sendMail.mock.calls[0][3]).toBeInstanceOf(fs.ReadStream);

        fs.unlinkSync(UPLOAD_PATH + '/pdf/' + data.LicenseCode + '.pdf');
    });
})
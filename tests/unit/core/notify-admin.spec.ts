import {Mailer} from "../../../src/domain/interfaces";
import {CsvRow, CsvRowStatus, Roles} from "../../../src/domain/models";
import {notifyAdmin} from "../../../src/core";

describe('NotifyPartner', () => {

    it('should be created', () => {
        const mailer: Mailer | any = {
            sendMail: jest.fn()
        };
        const results: CsvRow[] = [
            {
                status: CsvRowStatus.Success,
                data: {
                    Age: 30,
                    Email: "ronny@fretelweb.com",
                    FirstName: "Ronny",
                    LastName: "Fretel",
                    LicenseCode: "test-76432-234-234-234",
                    LicenseExpirationDate: new Date(),
                    LicenseRegistrationDate: new Date(),
                    Password: "sad34534",
                    Role: Roles.CYCLIST,
                    Username: "RFretel"
                }
            },
            {
                status: CsvRowStatus.Failed,
                data: {
                    Age: 30,
                    Email: "ronny@fretelweb.com",
                    FirstName: "Ronny",
                    LastName: "Fretel",
                    LicenseCode: "test-76432-234-234-234",
                    LicenseExpirationDate: new Date(),
                    LicenseRegistrationDate: new Date(),
                    Password: "sad34534",
                    Role: Roles.CYCLIST,
                    Username: "RFretel"
                }
            },
            {
                status: CsvRowStatus.Success,
                data: {
                    Age: 30,
                    Email: "ronny@fretelweb.com",
                    FirstName: "Ronny",
                    LastName: "Fretel",
                    LicenseCode: "test-76432-234-234-234",
                    LicenseExpirationDate: new Date(),
                    LicenseRegistrationDate: new Date(),
                    Password: "sad34534",
                    Role: Roles.CYCLIST,
                    Username: "RFretel"
                }
            },
        ]
        notifyAdmin('ronny@fretelweb.com', results, mailer);

        expect(mailer.sendMail).toHaveBeenCalled();
        expect(mailer.sendMail.mock.calls[0][0]).toContain('ronny@fretelweb.com');
        expect(mailer.sendMail.mock.calls[0][2]).toContain('Success: 2');
        expect(mailer.sendMail.mock.calls[0][2]).toContain('Failed: 1');
    });
})
import {Mailer} from "../../../src/domain/interfaces";
import {notifyPartner} from "../../../src/core";
import {Certificate, CsvRow, CsvRowStatus} from "../../../src/domain/models";

describe('NotifyPartner', () => {

    it('should be created', () => {
        const mailer: Mailer | any = {
            sendMail: jest.fn()
        };
        const results: CsvRow[] = [
            {status: CsvRowStatus.Success, data: {} as Certificate},
            {status: CsvRowStatus.Success, data: {} as Certificate},
            {status: CsvRowStatus.Failed, data: {} as Certificate},
            {status: CsvRowStatus.Success, data: {} as Certificate},
        ];
        notifyPartner('ronny@fretelweb.com', '1234567890', results, mailer);
        expect(mailer.sendMail).toHaveBeenCalled();
        expect(mailer.sendMail.mock.calls[0][0]).toEqual('ronny@fretelweb.com');
        expect(mailer.sendMail.mock.calls[0][1]).toEqual('PARTNER Process Completed');
    });
})
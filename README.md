## Velocitech Work

### Postman Documentation

https://documenter.getpostman.com/view/916537/2s8Ysp2vhP

### Endpoints

```
POST /api/upload
header: "REQUIRED: Authorization"
body: 
"REQUIRED: field file as form-data",
"REQUIRED: field email as form-data",
```

```
GET /api/consult?token=
header: "REQUIRED: Authorization"
query:  "REQUIRED: token as query param"
```

### Docker

```
# docker-composer run -d
```

Mailhog runs on port 8025


### Screenshots

<a href="https://ibb.co/7kgcQCf"><img src="https://i.ibb.co/PDTpMjL/2022-11-24-21-03.png" alt="2022-11-24-21-03" border="0" /></a>

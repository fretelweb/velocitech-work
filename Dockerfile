FROM node:14
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --quiet
COPY . .

EXPOSE 3000

CMD [ "npm","run","start" ]
import fs from "fs";
import {UPLOAD_PATH} from "../../configs";
import {CsvRow} from "../../domain/models";

const resolveFileToken = (token: String, results: CsvRow[]) => {
    if (!fs.existsSync(UPLOAD_PATH + '/done'))
        fs.mkdirSync(UPLOAD_PATH + '/done', {recursive: true})

    fs.createWriteStream(`${UPLOAD_PATH}/done/${token}.json`)
        .write(JSON.stringify(results))

    fs.unlinkSync(UPLOAD_PATH + '/csv/' + token + '.json')
    fs.unlinkSync(UPLOAD_PATH + '/csv/' + token)
}
export {resolveFileToken}
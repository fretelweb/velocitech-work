import {Mailer} from "../../domain/interfaces";
import nodemailer, {Transporter} from "nodemailer";
import {environment} from "../../configs";
import SMTPTransport from "nodemailer/lib/smtp-transport";

class MailerSmtp implements Mailer {
    transporter: Transporter;

    constructor() {
        const {smtp} = environment;
        this.transporter = nodemailer.createTransport({
            from: smtp.from,
            host: smtp.host,
            port: smtp.port,
            auth: {
                user: smtp.user,
                pass: smtp.pass
            },
            // secure: smtp.secure,
            // requireTLS: false,
        } as SMTPTransport.Options)
    }

    async sendMail(to: string, subject: string, body: string, attach?: any): Promise<any> {
        return await this.transporter.sendMail({
            from: environment.smtp.from,
            to: to,
            subject: subject,
            html: body,
            attachments: attach
        });
    }

}

export {MailerSmtp}
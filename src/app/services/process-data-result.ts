import {CsvRow, CsvRowStatus} from "../../domain/models";
import {Mailer} from "../../domain/interfaces";
import fs from "fs";
import {environment, UPLOAD_PATH} from "../../configs";
import {pdfCreator} from "./pdf-creator";
import {notifyAdmin, notifyHolder, notifyPartner} from "../../core";
import {resolveFileToken} from "./resolve-file-token";

const processDataResult = (token: string, results: CsvRow[], partnerEmail: string, mailer: Mailer): Promise<boolean> => {

    fs.createWriteStream(UPLOAD_PATH + '/csv/' + token + '.json').write(JSON.stringify(results))

    return new Promise(async (resolve, reject) => {
        for (const result of results) {
            const {data} = result
            try {
                const pdfPath = pdfCreator(data)
                const pdf = fs.createReadStream(pdfPath)
                await notifyHolder(data, pdf, mailer)
                result.status = CsvRowStatus.Success
            } catch (e) {
                result.status = CsvRowStatus.Failed
            }
        }

        await notifyPartner(partnerEmail, token, results, mailer)
        await notifyAdmin(environment.adminEmail, results, mailer)

        const allSuccess = results.every(result => result.status === CsvRowStatus.Success)
        if (allSuccess) {
            resolveFileToken(token, results)
            resolve(true)
        }
        else {
            reject(false)
        }
    })
};

export {processDataResult}
import * as fs from "fs";
import csvParser from "csv-parser";
import {Certificate, CsvRow, CsvRowStatus} from "../../domain/models";


const parserCsv = (filePath: string): Promise<CsvRow[]> => {
    return new Promise((resolve, reject) => {
        const result: CsvRow[] = []
        fs.createReadStream(filePath)
            .pipe(csvParser())
            .on('data', (row: Certificate) => result.push({
                status: CsvRowStatus.Pending,
                data: row
            }))
            .on('end', () => resolve(result))
            .on('error', (err) => reject(err))
    });
}
export {parserCsv}
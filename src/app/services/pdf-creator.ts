import {Certificate} from "../../domain/models"
import * as fs from "fs";
import {jsPDF} from "jspdf";
import {BASE_PATH, UPLOAD_PATH} from "../../configs";

const pdfCreator = (data: Certificate): string => {
    const fileName = `${data.LicenseCode}.pdf`;
    const pdfPath = `${UPLOAD_PATH}/pdf/`;

    if (!fs.existsSync(pdfPath))
        fs.mkdirSync(pdfPath, {recursive: true});

    const img = fs.readFileSync(
        `${BASE_PATH}/assets/card-bg.jpg`, {encoding: 'base64'});

    const doc = new jsPDF({
        orientation: 'landscape',
        format: [85.6, 54],
        unit: 'mm',
    });
    doc.setFontSize(9)
    doc.fill()
    const licence = data.LicenseCode.toUpperCase();
    doc.addImage(img, 'JPEG', 0, 0, 85.6, 54);
    doc.text("RIDER'S LICENCE " + data.Role, 20, 5)
        .text('1.  ' + data.LastName, 20, 10)
        .text('2.  ' + data.FirstName, 20, 15)
        .text('3.  ' + data.Age, 20, 20)
        .text('4a. ' + data.LicenseRegistrationDate, 20, 25)
        .text('4b. ' + data.LicenseExpirationDate, 20, 30)
        .text('5.  ' + licence.replace('-', '').slice(-8), 20, 35)
        .text('9. ' + licence, 5, 50)
    try {
        doc.save(pdfPath + fileName);
        return pdfPath + fileName;
    } catch (e) {
        throw new Error('Error saving pdf file');
    }

}

export {pdfCreator}
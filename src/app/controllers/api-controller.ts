import {Request, Response} from "express"
import * as fs from "fs"
import {MailerSmtp, parserCsv, processDataResult} from "../services";


const apiController = {

    async postUpload(req: Request, res: Response) {
        const {file, body} = req
        if (!file)
            return res.status(400).json({message: "No file uploaded"})
        if (!body.email)
            return res.status(400).json({message: "No email field provided"})

        const token = file.filename.split('.').shift()
        const results = await parserCsv(file.path)
        const rows = results.length

        const mailer = new MailerSmtp() // Reemplazar por DI
        processDataResult(token, results, body.email, mailer)
            .catch(console.log);

        res.status(201).json({
            message: "File Uploaded Successfully",
            data: {
                token: token,
                rows: rows,
                consult: `${req.protocol}://${req.get('host')}/api/consult?token=${token}`
            },
        })
    },

    async getConsult(req: Request, res: Response) {
        const token = req.query.token
        if (!token)
            return res.status(400).json({message: "Token not provided"})

        let filePath = 'uploads/done/' + token + '.json'
        if (fs.existsSync(filePath))
            return res.status(200).json({message: "File already processed"})

        filePath = 'uploads/csv/' + token + '.json'
        if (fs.existsSync(filePath))
            return res.status(200).json({message: "File processing"})

        filePath = 'uploads/csv/' + token
        if (!fs.existsSync(filePath))
            return res.status(404).json({message: "File not found"})
        else
            return res.status(200).json({message: "File queued for processing"})


    }
}

export {apiController}
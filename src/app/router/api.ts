import {Router} from "express";
import {apiController} from "../controllers";
import {authenticate, upload} from "../middlewares";

const apiRouter = Router();
apiRouter.use(authenticate);

apiRouter.post("/upload", upload('/csv').single("file"), apiController.postUpload);
apiRouter.get("/consult", apiController.getConsult);

export {apiRouter};

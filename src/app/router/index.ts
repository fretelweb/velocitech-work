import {Router} from 'express'
import {apiRouter} from "./api"
import {environment} from "../../configs";

const router = Router()

router.use('/api', apiRouter)
router.use("/", (req, res) => {
    res.json({
        message: "POST /api/upload to upload a file",
        header: "REQUIRED: Authorization",
        body: "REQUIRED: file",
        envs: environment
    })
});

export {router}
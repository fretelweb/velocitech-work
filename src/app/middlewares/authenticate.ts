import {NextFunction, Request, Response} from "express";

const authenticate = (req: Request, res: Response, next: NextFunction) => {

    const authorization = req.headers.authorization;
    if (!authorization)
        return res.status(401).json({message: "Unauthorized"});

    const token = authorization.split(" ")[1];
    if (!token)
        return res.status(401).json({message: "Access Denied"});

    next();
};

export {authenticate};

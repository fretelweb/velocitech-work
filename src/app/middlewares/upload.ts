import multer, {FileFilterCallback} from "multer";
import {Request} from "express";
import * as fs from "fs";
import {UPLOAD_PATH} from "../../configs";

const fileFilter = (req: Request, file: Express.Multer.File, callback: FileFilterCallback) => {
    if (!file.originalname.match(/\.(csv)$/))
        return callback(undefined, false);
    callback(undefined, true);
}

const upload = (dest) => {
    if (!fs.existsSync(UPLOAD_PATH + dest))
        fs.mkdirSync(UPLOAD_PATH + dest, {recursive: true})
    return multer({
        fileFilter,
        dest: 'uploads' + (dest ?? '')
    });
};

export {upload}

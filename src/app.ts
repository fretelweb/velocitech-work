import express from "express";
import {router} from "./app/router";
import cors from "cors";
import {environment} from "./configs";

const app = express();

app.use(cors());
app.use(express.json());
app.use("/", router);

app.listen(environment.port, () => {
    // console.log(`Server is running port ${environment.port} !`)
});

export {app};
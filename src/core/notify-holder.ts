import {Mailer} from "../domain/interfaces";
import {Certificate} from "../domain/models";
import * as fs from "fs";

const notifyHolder = (data: Certificate, attach: fs.ReadStream, mailer: Mailer) => {
    const body = `
        <h1>Rider License Certificate</h1>
        <p>Dear ${data.FirstName} ${data.LastName}</p>
        <p>Username: ${data.Username}</p>
                `;
    return mailer.sendMail(data.Email, 'HOLDER Rider License Certificate', body, attach);
}

export {notifyHolder}
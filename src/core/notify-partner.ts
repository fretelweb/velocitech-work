import {Mailer} from "../domain/interfaces";
import {CsvRow, CsvRowStatus} from "../domain/models";

const notifyPartner = async (partnerEmail: string, token: string, result: CsvRow[], mailer: Mailer) => {

    const body = `
        <h1>CSV with token ${token} has been completed.</h1>
        <p>${result.length} rows processed.</p>
        <p>${result.filter(r => r.status === CsvRowStatus.Success).length} rows valid.</p>
        <p>${result.filter(r => r.status === CsvRowStatus.Failed).length} rows failed.</p>`;

    return mailer.sendMail(
        partnerEmail,
        'PARTNER Process Completed',
        body,
    );
}

export {notifyPartner}
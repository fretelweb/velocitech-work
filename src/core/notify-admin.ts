import {CsvRow, CsvRowStatus} from "../domain/models";
import {Mailer} from "../domain/interfaces";

const notifyAdmin = (adminEmail: string, results: CsvRow[], mailer: Mailer) => {
    const fails = results.filter((r) => r.status === CsvRowStatus.Failed);
    const success = results.filter((r) => r.status === CsvRowStatus.Success);

    const body = `
        <h1>Report Certificates Delivered!</h1>
        <p>Success: ${success.length}</p>
        <p>Failed: ${fails.length}</p>
    `;

    return mailer.sendMail(adminEmail, 'ADMIN Certification Process', body);
}

export {notifyAdmin}
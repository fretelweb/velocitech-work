interface Mailer {
    sendMail(to: string, subject: string, body: string, attach?: any): Promise<boolean>;
}

export {Mailer};
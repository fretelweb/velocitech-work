import {Certificate} from "./Certificate";

enum CsvRowStatus {
    Pending = 'pending',
    Success = 'success',
    Failed = 'failed'
}

interface CsvRow {
    status: CsvRowStatus,
    data: Certificate
}

export {CsvRow, CsvRowStatus}
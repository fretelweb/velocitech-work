enum Roles {
    ADMIN = "ADMIN",
    CYCLIST = "CYCLIST",
}

interface Certificate {
    Username: string;
    Password: string;
    Role: Roles;
    LicenseRegistrationDate: Date;
    LicenseExpirationDate: Date;
    LicenseCode: string;
    FirstName: string;
    LastName: string;
    Age: number;
    Email: string;
}

export {Certificate, Roles}
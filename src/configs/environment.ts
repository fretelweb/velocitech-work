import path from "path";
import dotenv from "dotenv";

const BASE_PATH = path.resolve(__dirname, '../');
const UPLOAD_PATH = path.resolve(__dirname, '../../uploads');
const TEST_PATH = path.resolve(__dirname, '../../tests');

dotenv.config({path: BASE_PATH + '/../.env'});

const environment = {
    port: process.env.PORT || 3000,
    adminEmail: process.env.ADMIN_EMAIL || 'ronny@fretelweb.com',
    smtp: {
        host: process.env.SMTP_HOST || 'smtp.mailtrap.io',
        port: process.env.SMTP_PORT || 2525,
        user: process.env.SMTP_USER || 'user',
        pass: process.env.SMTP_PASS || 'pass',
        secure: process.env.SMTP_SECURE === 'true' || false,
        from: process.env.SMTP_FROM || '',
    }

}

export {
    environment,
    BASE_PATH,
    UPLOAD_PATH,
    TEST_PATH
};